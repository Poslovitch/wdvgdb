# Wikidata Video Games Dashboard (wdvgdb)

**wdvgdb** (short for **Wikidata Video Games Dashboard**) is a demonstration website to explore the re-usability of video games-related data from Wikidata. 

Powered by [Wikidata](https://www.wikidata.org).
