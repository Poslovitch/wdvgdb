import functools
from dataclasses import dataclass
from typing import Dict, List


@dataclass
class Critic:
    id: str
    stats: Dict[str, str]

    def total_games_reviewed(self) -> int:
        return functools.reduce(lambda a, b: int(a) + int(b), self.stats.values())

    def stats_labels(self) -> List[str]:
        return list(self.stats.keys())

    def stats_values(self) -> List[str]:
        return list(self.stats.values())
