import re
from typing import Dict, List, Tuple, Final

import SPARQLWrapper
import flask
import mwapi
from flask import Flask

import videogame
from critic import Critic

app = Flask(__name__)
app.config["DEBUG"] = True

user_agent: Final[str] = 'wdvgdb (wdvgdb@floriancuny.fr, https://codeberg.org/Poslovitch/wdvgdb)'
api_session = mwapi.Session('https://www.wikidata.org', user_agent=user_agent)

sparql_session = SPARQLWrapper.SPARQLWrapper('https://query.wikidata.org/sparql',
                                             agent=user_agent)
sparql_session.setReturnFormat(SPARQLWrapper.JSON)

PROP_REVIEW_SCORE: Final[str] = 'P444'
PROP_REVIEWER: Final[str] = 'P447'
PROP_DEVELOPER: Final[str] = 'P178'
PROP_DATE: Final[str] = 'P585'
PROP_PLATFORM: Final[str] = 'P400'


# Thanks, Lucas Werkmeister for this one!
def get_labels(item_ids: List[str]) -> Dict[str, str]:
    labels = {}

    for chunk in [item_ids[i:i + 50] for i in range(0, len(item_ids), 50)]:
        labels_response = api_session.get(action='wbgetentities',
                                          ids=chunk,
                                          props=['labels'],
                                          languages=['en'],
                                          languagefallback=1)
        for entity_id, entity in labels_response['entities'].items():
            labels[entity_id] = entity['labels']['en']['value']

    return labels


@app.route('/')
def p_index():
    query = """SELECT (COUNT(*) AS ?count) { ?item wdt:P31 wd:Q7889. }"""
    sparql_session.setQuery(query)
    result = sparql_session.query().convert()
    return flask.render_template('index.html', count=result['results']['bindings'][0]['count']['value'])


@app.route('/vg/Q<int:game_id>')
def p_video_game(game_id: int):
    item_response = api_session.get(action='wbgetentities',
                                    ids=[f"Q{game_id}"],
                                    props=['claims'])
    # return item_response
    video_game, required_labels = videogame.from_item(item_response['entities'])
    return flask.render_template('video_game.html', video_game=video_game, labels=get_labels(required_labels))


@app.route('/critic/Q<int:critic_id>')
def p_critic(critic_id):
    item_id = f"Q{critic_id}"
    query = """SELECT ?note (COUNT(?item) AS ?count) WHERE {
  ?item p:P444 ?review.
  ?review ps:P444 ?note; pq:P447 wd:%s.
}
GROUP BY ?note
ORDER BY ?note
""" % item_id
    sparql_session.setQuery(query)
    result = sparql_session.query().convert()
    stats: Dict[str, str] = {}
    for binding in result['results']['bindings']:
        stats[binding['note']['value']] = binding['count']['value']

    stats = {key: value for key, value in sorted(stats.items(), key=sort_stats)}
    critic = Critic(id=item_id, stats=stats)

    return flask.render_template('critic.html', critic=critic, labels=get_labels([item_id]))


def sort_stats(entry: Tuple[str, str]) -> float:
    """
    Sorts the stats based on the note value
    :param entry:
    :return: 0 if this is not a x/y note, ... otherwise
    """
    key = entry[0]
    result = re.match(r"(\d+(?:\.\d+)?)/\d+", key)
    if result:
        return float(result.group(1))
    else:
        return 0


if __name__ == '__main__':
    app.run()
