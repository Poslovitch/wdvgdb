from dataclasses import dataclass
from typing import Dict, List

import app


@dataclass
class Review:
    """
    Represents a critics review of a video game
    """
    reviewer: str
    note: str
    date: str
    platform: str


@dataclass
class VideoGame:
    id: str
    developers: List[str]
    reviews: List[Review]


def from_item(item: Dict) -> (VideoGame, List[str]):
    """

    :param item: The item from the MWAPI response
    :return: the video game and a list of labels to get
    """
    item_id = next(iter(item))
    item = item[item_id]
    reviews = get_reviews(item)
    developers = get_developers(item)

    # get labels to get from the reviews
    reviews_labels = []
    for entry in reviews:
        if entry.platform is not None and entry.platform not in reviews_labels:
            reviews_labels.append(entry.platform)
        if entry.reviewer is not None and entry.reviewer not in reviews_labels:
            reviews_labels.append(entry.reviewer)

    return VideoGame(id=item_id, reviews=reviews, developers=developers), [item_id] + reviews_labels + developers


def get_reviews(item: Dict) -> List[Review]:
    res = []
    claims = item['claims']
    if app.PROP_REVIEW_SCORE in claims:
        for review in claims[app.PROP_REVIEW_SCORE]:
            qualifiers: Dict[str, List] = review['qualifiers']  # Dict of qualifiers for this review

            note = review['mainsnak']['datavalue']['value']
            reviewer = qualifiers[app.PROP_REVIEWER][0]['datavalue']['value']['id']
            date = None
            platform = None
            if app.PROP_DATE in qualifiers:
                date = qualifiers[app.PROP_DATE][0]['datavalue']['value']['time']
            if app.PROP_PLATFORM in qualifiers:
                platform = qualifiers[app.PROP_PLATFORM][0]['datavalue']['value']['id']

            res.append(Review(reviewer=reviewer, note=note, date=date, platform=platform))
    return res


def get_developers(item: Dict) -> List[str]:
    res = []
    claims = item['claims']
    if app.PROP_DEVELOPER in claims:
        for developer in claims[app.PROP_DEVELOPER]:
            developer_id = developer['mainsnak']['datavalue']['value']['id']
            res.append(developer_id)
    return res
